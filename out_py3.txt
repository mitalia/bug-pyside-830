[0] k -> PySide2.QtCore.Qt.Key.Key_1 (hex: 0x31)
[1] k -> PySide2.QtCore.Qt.Key.Key_1 (hex: 0x31)
[2] k -> PySide2.QtCore.Qt.Key.Key_1 (hex: 0x31)
[3] k -> PySide2.QtCore.Qt.Key.Key_1 (hex: 0x31)

[0] int(k) -> 49 (hex: 0x31)
[1] int(k) -> 49 (hex: 0x31)
[2] int(k) -> 49 (hex: 0x31)
[3] int(k) -> 49 (hex: 0x31)

[0] k + 0 -> 49 (hex: 0x31)
[1] k + 0 -> 49 (hex: 0x31)
[2] k + 0 -> 49 (hex: 0x31)
[3] k + 0 -> 49 (hex: 0x31)

[0] k + 1 -> 50 (hex: 0x32)
[1] k + 1 -> 50 (hex: 0x32)
[2] k + 1 -> 50 (hex: 0x32)
[3] k + 1 -> 50 (hex: 0x32)

[0] k + 2 -> 51 (hex: 0x33)
[1] k + 2 -> 51 (hex: 0x33)
[2] k + 2 -> 51 (hex: 0x33)
[3] k + 2 -> 51 (hex: 0x33)

[0] k + 3 -> 52 (hex: 0x34)
[1] k + 3 -> 52 (hex: 0x34)
[2] k + 3 -> 52 (hex: 0x34)
[3] k + 3 -> 52 (hex: 0x34)

[0] k + -1 -> 48 (hex: 0x30)
[1] k + -1 -> 48 (hex: 0x30)
[2] k + -1 -> 48 (hex: 0x30)
[3] k + -1 -> 48 (hex: 0x30)

[0] k + -2 -> 47 (hex: 0x2f)
[1] k + -2 -> 47 (hex: 0x2f)
[2] k + -2 -> 47 (hex: 0x2f)
[3] k + -2 -> 47 (hex: 0x2f)

[0] k + -3 -> 46 (hex: 0x2e)
[1] k + -3 -> 46 (hex: 0x2e)
[2] k + -3 -> 46 (hex: 0x2e)
[3] k + -3 -> 46 (hex: 0x2e)

[0] k * 0 -> 0 (hex: 0x0)
[1] k * 0 -> 0 (hex: 0x0)
[2] k * 0 -> 0 (hex: 0x0)
[3] k * 0 -> 0 (hex: 0x0)

[0] k * 1 -> 49 (hex: 0x31)
[1] k * 1 -> 49 (hex: 0x31)
[2] k * 1 -> 49 (hex: 0x31)
[3] k * 1 -> 49 (hex: 0x31)

[0] k * 2 -> 98 (hex: 0x62)
[1] k * 2 -> 98 (hex: 0x62)
[2] k * 2 -> 98 (hex: 0x62)
[3] k * 2 -> 98 (hex: 0x62)

[0] k * 3 -> 147 (hex: 0x93)
[1] k * 3 -> 147 (hex: 0x93)
[2] k * 3 -> 147 (hex: 0x93)
[3] k * 3 -> 147 (hex: 0x93)

[0] k * -1 -> -49 (hex: 0x-31)
[1] k * -1 -> -49 (hex: 0x-31)
[2] k * -1 -> -49 (hex: 0x-31)
[3] k * -1 -> -49 (hex: 0x-31)

[0] k * -2 -> -98 (hex: 0x-62)
[1] k * -2 -> -98 (hex: 0x-62)
[2] k * -2 -> -98 (hex: 0x-62)
[3] k * -2 -> -98 (hex: 0x-62)

[0] k * -3 -> -147 (hex: 0x-93)
[1] k * -3 -> -147 (hex: 0x-93)
[2] k * -3 -> -147 (hex: 0x-93)
[3] k * -3 -> -147 (hex: 0x-93)

[0] 0 + k -> 49 (hex: 0x31)
[1] 0 + k -> 49 (hex: 0x31)
[2] 0 + k -> 49 (hex: 0x31)
[3] 0 + k -> 49 (hex: 0x31)

[0] 1 + k -> 50 (hex: 0x32)
[1] 1 + k -> 50 (hex: 0x32)
[2] 1 + k -> 50 (hex: 0x32)
[3] 1 + k -> 50 (hex: 0x32)

[0] 2 + k -> 50 (hex: 0x32)
[1] 2 + k -> 50 (hex: 0x32)
[2] 2 + k -> 50 (hex: 0x32)
[3] 2 + k -> 50 (hex: 0x32)

[0] 3 + k -> 50 (hex: 0x32)
[1] 3 + k -> 50 (hex: 0x32)
[2] 3 + k -> 50 (hex: 0x32)
[3] 3 + k -> 50 (hex: 0x32)

[0] -1 + k -> 48 (hex: 0x30)
[1] -1 + k -> 48 (hex: 0x30)
[2] -1 + k -> 48 (hex: 0x30)
[3] -1 + k -> 48 (hex: 0x30)

[0] -2 + k -> 48 (hex: 0x30)
[1] -2 + k -> 48 (hex: 0x30)
[2] -2 + k -> 48 (hex: 0x30)
[3] -2 + k -> 48 (hex: 0x30)

[0] -3 + k -> 48 (hex: 0x30)
[1] -3 + k -> 48 (hex: 0x30)
[2] -3 + k -> 48 (hex: 0x30)
[3] -3 + k -> 48 (hex: 0x30)

[0] 0 * k -> 0 (hex: 0x0)
[1] 0 * k -> 0 (hex: 0x0)
[2] 0 * k -> 0 (hex: 0x0)
[3] 0 * k -> 0 (hex: 0x0)

[0] 1 * k -> 49 (hex: 0x31)
[1] 1 * k -> 49 (hex: 0x31)
[2] 1 * k -> 49 (hex: 0x31)
[3] 1 * k -> 49 (hex: 0x31)

[0] 2 * k -> 49 (hex: 0x31)
[1] 2 * k -> 49 (hex: 0x31)
[2] 2 * k -> 49 (hex: 0x31)
[3] 2 * k -> 49 (hex: 0x31)

[0] 3 * k -> 49 (hex: 0x31)
[1] 3 * k -> 49 (hex: 0x31)
[2] 3 * k -> 49 (hex: 0x31)
[3] 3 * k -> 49 (hex: 0x31)

[0] -1 * k -> -49 (hex: 0x-31)
[1] -1 * k -> -49 (hex: 0x-31)
[2] -1 * k -> -49 (hex: 0x-31)
[3] -1 * k -> -49 (hex: 0x-31)

[0] -2 * k -> -49 (hex: 0x-31)
[1] -2 * k -> -49 (hex: 0x-31)
[2] -2 * k -> -49 (hex: 0x-31)
[3] -2 * k -> -49 (hex: 0x-31)

[0] -3 * k -> -49 (hex: 0x-31)
[1] -3 * k -> -49 (hex: 0x-31)
[2] -3 * k -> -49 (hex: 0x-31)
[3] -3 * k -> -49 (hex: 0x-31)

[0] k + 0.0 -> 49 (hex: 0x31)
[1] k + 0.0 -> 49 (hex: 0x31)
[2] k + 0.0 -> 49 (hex: 0x31)
[3] k + 0.0 -> 49 (hex: 0x31)

[0] k + 1.0 -> 50 (hex: 0x32)
[1] k + 1.0 -> 50 (hex: 0x32)
[2] k + 1.0 -> 50 (hex: 0x32)
[3] k + 1.0 -> 50 (hex: 0x32)

[0] k + 1.5 -> 50 (hex: 0x32)
[1] k + 1.5 -> 50 (hex: 0x32)
[2] k + 1.5 -> 50 (hex: 0x32)
[3] k + 1.5 -> 50 (hex: 0x32)

[0] k + 2.0 -> 51 (hex: 0x33)
[1] k + 2.0 -> 51 (hex: 0x33)
[2] k + 2.0 -> 51 (hex: 0x33)
[3] k + 2.0 -> 51 (hex: 0x33)

[0] k + -1.0 -> 48 (hex: 0x30)
[1] k + -1.0 -> 48 (hex: 0x30)
[2] k + -1.0 -> 48 (hex: 0x30)
[3] k + -1.0 -> 48 (hex: 0x30)

[0] k + -1.5 -> 48 (hex: 0x30)
[1] k + -1.5 -> 48 (hex: 0x30)
[2] k + -1.5 -> 48 (hex: 0x30)
[3] k + -1.5 -> 48 (hex: 0x30)

[0] k + -2.0 -> 47 (hex: 0x2f)
[1] k + -2.0 -> 47 (hex: 0x2f)
[2] k + -2.0 -> 47 (hex: 0x2f)
[3] k + -2.0 -> 47 (hex: 0x2f)

[0] 0.0 + k -> 49 (hex: 0x31)
[1] 0.0 + k -> 49 (hex: 0x31)
[2] 0.0 + k -> 49 (hex: 0x31)
[3] 0.0 + k -> 49 (hex: 0x31)

[0] 1.0 + k -> 4607182418800017457 (hex: 0x3ff0000000000031)
[1] 1.0 + k -> 4607182418800017457 (hex: 0x3ff0000000000031)
[2] 1.0 + k -> 4607182418800017457 (hex: 0x3ff0000000000031)
[3] 1.0 + k -> 4607182418800017457 (hex: 0x3ff0000000000031)

[0] 1.5 + k -> 4609434218613702705 (hex: 0x3ff8000000000031)
[1] 1.5 + k -> 4609434218613702705 (hex: 0x3ff8000000000031)
[2] 1.5 + k -> 4609434218613702705 (hex: 0x3ff8000000000031)
[3] 1.5 + k -> 4609434218613702705 (hex: 0x3ff8000000000031)

[0] 2.0 + k -> 4611686018427387953 (hex: 0x4000000000000031)
[1] 2.0 + k -> 4611686018427387953 (hex: 0x4000000000000031)
[2] 2.0 + k -> 4611686018427387953 (hex: 0x4000000000000031)
[3] 2.0 + k -> 4611686018427387953 (hex: 0x4000000000000031)

[0] -1.0 + k -> -4616189618054758351 (hex: 0x-400fffffffffffcf)
[1] -1.0 + k -> -4616189618054758351 (hex: 0x-400fffffffffffcf)
[2] -1.0 + k -> -4616189618054758351 (hex: 0x-400fffffffffffcf)
[3] -1.0 + k -> -4616189618054758351 (hex: 0x-400fffffffffffcf)

[0] -1.5 + k -> -4613937818241073103 (hex: 0x-4007ffffffffffcf)
[1] -1.5 + k -> -4613937818241073103 (hex: 0x-4007ffffffffffcf)
[2] -1.5 + k -> -4613937818241073103 (hex: 0x-4007ffffffffffcf)
[3] -1.5 + k -> -4613937818241073103 (hex: 0x-4007ffffffffffcf)

[0] -2.0 + k -> -4611686018427387855 (hex: 0x-3fffffffffffffcf)
[1] -2.0 + k -> -4611686018427387855 (hex: 0x-3fffffffffffffcf)
[2] -2.0 + k -> -4611686018427387855 (hex: 0x-3fffffffffffffcf)
[3] -2.0 + k -> -4611686018427387855 (hex: 0x-3fffffffffffffcf)

[0] k + "0" -> 49 (hex: 0x31)
[1] k + "0" -> 49 (hex: 0x31)
[2] k + "0" -> 49 (hex: 0x31)
[3] k + "0" -> 49 (hex: 0x31)

[0] k + "1" -> 50 (hex: 0x32)
[1] k + "1" -> 50 (hex: 0x32)
[2] k + "1" -> 50 (hex: 0x32)
[3] k + "1" -> 50 (hex: 0x32)

[0] k + "2" -> 51 (hex: 0x33)
[1] k + "2" -> 51 (hex: 0x33)
[2] k + "2" -> 51 (hex: 0x33)
[3] k + "2" -> 51 (hex: 0x33)

[0] k + "22" -> 71 (hex: 0x47)
[1] k + "22" -> 71 (hex: 0x47)
[2] k + "22" -> 71 (hex: 0x47)
[3] k + "22" -> 71 (hex: 0x47)

[0] k + "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[1] k + "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[2] k + "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[3] k + "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set

[0] k + "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[1] k + "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[2] k + "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[3] k + "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set

[0] k * "0" -> 0 (hex: 0x0)
[1] k * "0" -> 0 (hex: 0x0)
[2] k * "0" -> 0 (hex: 0x0)
[3] k * "0" -> 0 (hex: 0x0)

[0] k * "1" -> 49 (hex: 0x31)
[1] k * "1" -> 49 (hex: 0x31)
[2] k * "1" -> 49 (hex: 0x31)
[3] k * "1" -> 49 (hex: 0x31)

[0] k * "2" -> 98 (hex: 0x62)
[1] k * "2" -> 98 (hex: 0x62)
[2] k * "2" -> 98 (hex: 0x62)
[3] k * "2" -> 98 (hex: 0x62)

[0] k * "22" -> 1078 (hex: 0x436)
[1] k * "22" -> 1078 (hex: 0x436)
[2] k * "22" -> 1078 (hex: 0x436)
[3] k * "22" -> 1078 (hex: 0x436)

[0] k * "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[1] k * "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[2] k * "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[3] k * "abcde" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set

[0] k * "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[1] k * "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[2] k * "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[3] k * "abcdef" -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set

[0] k * object() -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[1] k * object() -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[2] k * object() -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set
[3] k * object() -> exception!
SystemError: ../Objects/longobject.c:403: bad argument to internal function

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "enum_bug.py", line 85, in <module>
    val = eval(i)
SystemError: PyEval_EvalFrameEx returned a result with an error set

[0] "0" + k -> 50 (hex: 0x32)
[1] "0" + k -> 50 (hex: 0x32)
[2] "0" + k -> 50 (hex: 0x32)
[3] "0" + k -> 50 (hex: 0x32)

[0] "1" + k -> 50 (hex: 0x32)
[1] "1" + k -> 50 (hex: 0x32)
[2] "1" + k -> 50 (hex: 0x32)
[3] "1" + k -> 50 (hex: 0x32)

[0] "2" + k -> 50 (hex: 0x32)
[1] "2" + k -> 50 (hex: 0x32)
[2] "2" + k -> 50 (hex: 0x32)
[3] "2" + k -> 50 (hex: 0x32)

[0] "22" + k -> 51 (hex: 0x33)
[1] "22" + k -> 51 (hex: 0x33)
[2] "22" + k -> 51 (hex: 0x33)
[3] "22" + k -> 51 (hex: 0x33)

[0] "abcde" + k -> 54 (hex: 0x36)
[1] "abcde" + k -> 54 (hex: 0x36)
[2] "abcde" + k -> 54 (hex: 0x36)
[3] "abcde" + k -> 54 (hex: 0x36)

[0] "abcdef" + k -> 55 (hex: 0x37)
[1] "abcdef" + k -> 55 (hex: 0x37)
[2] "abcdef" + k -> 55 (hex: 0x37)
[3] "abcdef" + k -> 55 (hex: 0x37)

[0] object() + k -> 139629207089393 (hex: 0x7efdf549f0f1)
[1] object() + k -> 139629207089393 (hex: 0x7efdf549f0f1)
[2] object() + k -> 139629207089393 (hex: 0x7efdf549f0f1)
[3] object() + k -> 139629207089393 (hex: 0x7efdf549f0f1)

[0] "0" * k -> 49 (hex: 0x31)
[1] "0" * k -> 49 (hex: 0x31)
[2] "0" * k -> 49 (hex: 0x31)
[3] "0" * k -> 49 (hex: 0x31)

[0] "1" * k -> 49 (hex: 0x31)
[1] "1" * k -> 49 (hex: 0x31)
[2] "1" * k -> 49 (hex: 0x31)
[3] "1" * k -> 49 (hex: 0x31)

[0] "2" * k -> 49 (hex: 0x31)
[1] "2" * k -> 49 (hex: 0x31)
[2] "2" * k -> 49 (hex: 0x31)
[3] "2" * k -> 49 (hex: 0x31)

[0] "22" * k -> 98 (hex: 0x62)
[1] "22" * k -> 98 (hex: 0x62)
[2] "22" * k -> 98 (hex: 0x62)
[3] "22" * k -> 98 (hex: 0x62)

[0] "abcde" * k -> 245 (hex: 0xf5)
[1] "abcde" * k -> 245 (hex: 0xf5)
[2] "abcde" * k -> 245 (hex: 0xf5)
[3] "abcde" * k -> 245 (hex: 0xf5)

[0] "abcdef" * k -> 294 (hex: 0x126)
[1] "abcdef" * k -> 294 (hex: 0x126)
[2] "abcdef" * k -> 294 (hex: 0x126)
[3] "abcdef" * k -> 294 (hex: 0x126)

[0] object() * k -> 6841831147377856 (hex: 0x184e9bf32714c0)
[1] object() * k -> 6841831147377856 (hex: 0x184e9bf32714c0)
[2] object() * k -> 6841831147377856 (hex: 0x184e9bf32714c0)
[3] object() * k -> 6841831147377856 (hex: 0x184e9bf32714c0)

