from __future__ import print_function
from PySide2.QtCore import QCoreApplication, Qt
import sys
import traceback

a = QCoreApplication([])

k = Qt.Key.Key_1
tests = """\
k
int(k)
k + 0
k + 1
k + 2
k + 3
k + -1
k + -2
k + -3
k * 0
k * 1
k * 2
k * 3
k * -1
k * -2
k * -3
0 + k
1 + k
2 + k
3 + k
-1 + k
-2 + k
-3 + k
0 * k
1 * k
2 * k
3 * k
-1 * k
-2 * k
-3 * k
k + 0.0
k + 1.0
k + 1.5
k + 2.0
k + -1.0
k + -1.5
k + -2.0
0.0 + k
1.0 + k
1.5 + k
2.0 + k
-1.0 + k
-1.5 + k
-2.0 + k
k + "0"
k + "1"
k + "2"
k + "22"
k + "abcde"
k + "abcdef"
k * "0"
k * "1"
k * "2"
k * "22"
k * "abcde"
k * "abcdef"
k * object()
"0" + k
"1" + k
"2" + k
"22" + k
"abcde" + k
"abcdef" + k
object() + k
"0" * k
"1" * k
"2" * k
"22" * k
"abcde" * k
"abcdef" * k
object() * k\
""".split("\n")

magic = object()
for i in tests:
    oval = magic
    pcount = 0
    for j in range(4):
        try:
            val = eval(i)
            if val != oval:
                pcount += 1
                print("[%d] %s -> %r (hex: 0x%x)" % (j, i, val, val))
            oval = val
        except:
            pcount += 1
            print("[%d] %s -> exception!" % (j, i))
            traceback.print_exc(None, sys.stdout)
            oval = magic
    if pcount > 1:
        print()
